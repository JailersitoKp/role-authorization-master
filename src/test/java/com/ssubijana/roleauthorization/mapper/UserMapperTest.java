package com.ssubijana.roleauthorization.mapper;

import com.ssubijana.roleauthorization.domain.User;
import com.ssubijana.roleauthorization.web.presentation.AuthorizationRequest;
import com.ssubijana.roleauthorization.web.presentation.UserResponse;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
public class UserMapperTest {

	private static final long USER_ID = 1L;

	private static final String USER_NAME = "USER_NAME";

	@Test
	public void toResponseShouldReturnValidUserResponse() {
		User user = new User();
		user.setId(USER_ID);
		user.setName(USER_NAME);
		user.setPassword("USER_PASSWORD");

		UserResponse userResponse = UserMapper.toResponse(user);

		assertThat(userResponse.getId()).isEqualTo(user.getId());
		assertThat(userResponse.getName()).isEqualTo(user.getName());

	}

	@Test
	public void toDomainShouldReturnValidUser() {
		AuthorizationRequest authorizationRequest = new AuthorizationRequest();
		authorizationRequest.setUserName(USER_NAME);
		authorizationRequest.setPassword("USER_PASSWORD");

		User user = UserMapper.toDomain(authorizationRequest);

		assertThat(user.getName()).isEqualTo(authorizationRequest.getUserName());
		assertThat(user.getPassword()).isEqualTo(authorizationRequest.getPassword());

	}

}
