package com.ssubijana.roleauthorization.mapper;

import com.ssubijana.roleauthorization.domain.User;
import com.ssubijana.roleauthorization.web.presentation.AuthorizationRequest;
import com.ssubijana.roleauthorization.web.presentation.UserResponse;

public class UserMapper {

	private UserMapper() {
	}

	public static UserResponse toResponse(User user) {
		UserResponse userResponse = new UserResponse();
		userResponse.setName(user.getName());
		userResponse.setId(user.getId());
		user.getRoles().stream().forEach( r -> { userResponse.setRoles(userResponse.getRoles() + "," + r.getDescription()); });
		return userResponse;
	}

	public static User toDomain(AuthorizationRequest authorizationRequest) {
		User user = new User();
		user.setName(authorizationRequest.getUserName());
		user.setPassword(authorizationRequest.getPassword());
		return user;
	}
}
