package com.ssubijana.roleauthorization.service;

import com.ssubijana.roleauthorization.domain.Role;
import com.ssubijana.roleauthorization.domain.User;
import com.ssubijana.roleauthorization.repository.RoleRepository;
import com.ssubijana.roleauthorization.repository.UserRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashSet;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class UserServiceTest {

	private static final String USER_NAME = "USER_NAME";

	private static final long USER_ID = 1L;

	private static final String USER_PASS = "USER_PASS";

	@Mock
	private UserRepository userRepository;

	@Mock
	private RoleRepository roleRepository;

	@InjectMocks
	private UserServiceImpl userService;

	@Test
	public void loadUserByUsernameShouldMapUserDetails() {
		Set<Role> roles = new HashSet<>();
		roles.add(new Role(1L, "ROLE_ADMIN", "ROLE_DESC"));
		roles.add(new Role(2L, "ROLE_USER", "ROLE_DESC_USER"));

		User expectedUser = new User(USER_ID, USER_NAME, USER_PASS, roles);

		when(userRepository.findByName(USER_NAME)).thenReturn(expectedUser);

		final UserDetails userDetails = userService.loadUserByUsername(USER_NAME);

		assertThat(userDetails).isNotNull();
		assertThat(userDetails.getUsername()).isEqualTo(USER_NAME);
		assertThat(userDetails.getAuthorities()).isNotEmpty();
	}

	@Test(expected = UsernameNotFoundException.class)
	public void loadUserByUsernameShouldThrowExceptionIfUserIsNotFound() {
		userService.loadUserByUsername(USER_NAME);
	}

	@Test
	public void getUserShouldCallRepository() {
		userService.getUser(USER_ID);

		verify(userRepository).findById(USER_ID);
	}

	@Test
	public void saveUserShouldSaveUserWithUserRole() {
		final User userToSave = new User(0, USER_NAME, USER_PASS, null);
		final Role userRole = new Role(1L, "ROLE_USER", "ROLE_USER");

		Set<Role> roles = new HashSet<>();
		roles.add(userRole);
		User encryptedUser = new User(0, USER_NAME, USER_PASS, roles);

		when(roleRepository.findByName("USER")).thenReturn(userRole);
		when(userRepository.save(encryptedUser)).thenReturn(encryptedUser);

		final User savedUser = userService.save(userToSave);

		assertThat(savedUser).isEqualTo(encryptedUser);
	}

}
