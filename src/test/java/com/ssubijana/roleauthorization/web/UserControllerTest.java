package com.ssubijana.roleauthorization.web;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.ssubijana.roleauthorization.domain.User;
import com.ssubijana.roleauthorization.service.UserServiceImpl;
import com.ssubijana.roleauthorization.web.presentation.AuthorizationRequest;
import com.ssubijana.roleauthorization.web.presentation.UserResponse;

@RunWith(SpringJUnit4ClassRunner.class)
public class UserControllerTest {

	private static final long USER_ID = 1L;

	public static final String USER_NAME = "USER_NAME";

	public static final String USER_PASSWORD = "USER_PASSWORD";

	private static final String ENCRYPTED_PASSWORD = "ENCRYPTED_PASSWORD";

	@Mock
	private UserServiceImpl userService;

	@Mock
	private BCryptPasswordEncoder bCryptPasswordEncoder;

	@InjectMocks
	private UserController userController;

	@Test
	public void getUserShouldCallService() {
		final User user = new User(USER_ID, USER_NAME, USER_PASSWORD, null);
		when(userService.getUser(USER_ID)).thenReturn(user);
		final ResponseEntity<UserResponse> userResponse = userController.getUser(USER_ID);

		assertThat(userResponse.getBody()).isNotNull();

		UserResponse retrievedUser = userResponse.getBody();
		assertThat(retrievedUser.getId()).isEqualTo(user.getId());
		assertThat(retrievedUser.getName()).isEqualTo(user.getName());
	}

	@Test
	public void getUserShouldReturnNotFound() {
		final ResponseEntity<UserResponse> userResponse = userController.getUser(USER_ID);

		assertThat(userResponse.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
	}

	@Test
	public void saveUserShouldCallService() {
		final AuthorizationRequest authorizationRequest = new AuthorizationRequest();
		authorizationRequest.setUserName(USER_NAME);
		authorizationRequest.setPassword(USER_PASSWORD);
		final User userToSave = new User();
		userToSave.setName(USER_NAME);
		userToSave.setPassword(ENCRYPTED_PASSWORD);
		final User savedUser = new User();
		savedUser.setId(USER_ID);
		savedUser.setName(USER_NAME);
		savedUser.setPassword(USER_PASSWORD);

		when(bCryptPasswordEncoder.encode(USER_PASSWORD)).thenReturn(ENCRYPTED_PASSWORD);
		when(userService.save(userToSave)).thenReturn(savedUser);

		final ResponseEntity<User> userResponse = userController.saveUser(authorizationRequest);
		assertThat(userResponse.getBody()).isEqualTo(savedUser);
		assertThat(userResponse.getStatusCode()).isEqualTo(HttpStatus.OK);
	}

}
